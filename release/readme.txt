Covid version J-1.0 - november 5, 2020
=========================================

Documentation for this program can be found at the following locations:

1 - files in this directory:
    * readme.txt: this file, contains general information and how to start the program
    * changelog.txt: changes since previous versions 
    * input files in /input are example files
     
2 - the wiki for this project, at 
	https://gitlab.com/erasmusmc-public-health/covid.public/-/wikis/home. 
    
3 - Javadoc, at https://erasmgz.gitlab.io/covid/apidocs/. This contains code 
	comments, mostly interesting for programmers, but users may also find enough information here.
	(sorry, not yet on air at this time of writing, but will be fixed soon)

System requirements
===================

* at least 4GB on memory
* you must have java 8 installed. Beware that the program will NOT run on more recent versions than java 8. 
  Java 9, 10 and 11 WILL NOT WORK. 
  If you have need a higher version of java on your machine for other programs: it is perfectly sound to have
  different java versions installed on one machine. If java 8 is not your default java version, you must 
  call the java program with explicit path specification. For example in my linux system: 
  /usr/lib/jvm/java-8-openjdk-amd64/bin/java -jar ....
  Where java 8 is installed in the specified directory.


Running the program. 
====================

Running the program with input: 

java -jar covid.jar -iinput/covid.xml -oinput/outputDef.xml -r1-1 -b1M

Make sure the path indications for input files are correct; the path is relative for the directory in
which the program file is located. 
Also be aware that in the computer world, a file path with spaces in it will fail, unless you place it 
between "". This is because the computer will think that the file name ends when encountering the 
first space. So: -i"filename path with spaces.xml"

Output will be generated in the directory /output under the directory in which the program file 
is located.

If you need to run the program with a large number of agents: 
--------------------------------------------------------------
At present the program has been tested with 10 million persons, 10 superclusters, 10000 clusters, 350 days. 
That took about 20 minutes. There are still some tricks to apply, so future versions might be faster, though
probably not much.  
To run the simulation with a number of persons as large as this, you might need to reserve extra 
memory for java, depending on the system where you run it on. This is done as follows:  

java -Xmx7G -XX:-UseGCOverheadLimit -jar covid.jar -iinput/covid.xml -oinput/outputDef.xml -r0-1 -b1M

The extra parameters just after the word "java" tell java to use extra memory (7GB), and to switch off safety measures 
which tend to take unnecessary time. 

NEW: the -b switch: 
-------------------
You may have noticed that the program works in the same way as our other simulation programs, like wormsim
or ntdsim. However, there's one new command line option: the -b switch. 

This switch tells the program to use a printBuffer of the given size. The number after the 'b' gives the size
of the print buffer in bytes.
Our programs usually work with the strategy to store all output in memory, printing it out only at the end of a 
simulation run. As covid output is usually very big, this strategy would use up a lot of memory during a simulation run. 
Therefore, a buffer is used, and every time the buffer is full (during a simulation run), it is emptied and written to file. 
This feature can speed up the application's execution time considerably, but you'd have to experiment with the 
optimal buffer size setting. 
 
Tip: type java -jar covid.jar, and the program will give you explanation on all the 
command line parameters, including -b. 



WARNINGS
============
The program logs warnings to the console in case of any problems or strange situations
encountered. Please take these warnings serious. 

XML / DSX FILES
===============

The input files are all xml files. There is a general xml file and an outputDef.xml. 
Note that the outputDef.xml file hasn't much functionality yet, but it is added for 
future extensions. 

XML is eXtended Markup Language, and these files can be validated before use. There
is absolutely no need to do this, but if you made a lot of changes in the structure, 
it might be sensible to do this. 
Validating goes against xsd schema's. These schema's contain the definitions to which
an xml file should be validated. How to do this is beyond the scope of this documentation;
there is plenty of information on the internet on this. 
 
The schema files are included in the /input/schema directory. Even if you don't use
them for validation, they can be used as a form of documentation, to look up what 
is allowed and what is not allowed in the input files. 

	